package com.example.got;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vungle.warren.InitCallback;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;
import com.vungle.warren.VungleNativeAd;
import com.vungle.warren.error.VungleException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private RelativeLayout flexfeed_container;
    private VungleNativeAd vungleNativeAd;
    private View nativeAdView;
    private final String autocachePlacementReferenceID = "DEFAULT-6595425";
    private Button close_button;
    private final List<String> placementsList =
            Arrays.asList(autocachePlacementReferenceID, "DYNAMIC_TEMPLATE_INTERSTITIAL-6969365", "FLEX_FEED-2416159");

    final String app_id = "5ae0db55e2d43668c97bd65e";
    final String LOG_TAG = "MyComments";
    Button b ;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        b = (Button) findViewById(R.id.button1);
        close_button = (Button) findViewById(R.id.close_button);
        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vungleNativeAd.finishDisplayingAd();
                flexfeed_container.removeView(nativeAdView);
                vungleNativeAd = null;
                flexfeed_container.setVisibility(View.INVISIBLE);
                b.setVisibility(View.VISIBLE);
            }

        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initSDK();
                count++;
                if (Vungle.isInitialized() && count%2==0) {
                    Log.d("Filtering if" , "count "+count);
                    Vungle.loadAd(placementsList.get(1), vungleLoadAdCallback);
                    play();
                }
                else
                {
                    Log.d("Filtering else" , "count "+count + " Vungle.isInitialized() "  + Vungle.isInitialized());
                    TextView textView = (TextView) findViewById(R.id.textView);
                    textView.setHeight(320); textView.setWidth(320);
                    textView.setText(randomQuotes());
                }
            }
        });
    }

    private String randomQuotes()
    {
        ArrayList<String> list= new ArrayList<String>();
        Random r = new Random();
        list.add("The man who passes the sentence should swing the sword. — Ned Stark");
        list.add("The things I do for love. — Jaime Lannister");
        list.add("When you play the game of thrones, you win or you die. — Cerse");
        list.add("The night is dark and full of terrors. — Melisandre");
        list.add("Power resides where men believe it resides. It's a trick; a shadow on the wall. — Varys");
        list.add("The powerful have always preyed on the powerless. That’s how they became powerful in the first place. - Tyrion Lannister");
        list.add("A mind needs books like a sword needs a whetstone. That’s why I read so much. — Tyrion Lannister");
        list.add("Leave one wolf alive and the sheep are never safe. — Arya Stark");
        list.add("Nothing isn’t better or worse than anything. Nothing is just nothing. — Arya Stark");
        list.add("“Chaos isn’t a pit. Chaos is a ladder. — Lord Baelish");
        Log.d("quote",list.get(r.nextInt(10)));
        return  list.get(r.nextInt(10));

    }

    private void play() {
        if (Vungle.isInitialized() && Vungle.canPlayAd(placementsList.get(1))) {

            flexfeed_container = findViewById(R.id.flexfeedcontainer);
            flexfeed_container.setVisibility(View.VISIBLE);
            b.setVisibility(View.INVISIBLE);
            vungleNativeAd = Vungle.getNativeAd(placementsList.get(1), vunglePlayAdCallback);
            nativeAdView = vungleNativeAd.renderNativeView();
            flexfeed_container.addView(nativeAdView);

        }
    }


    private void initSDK() {
        Vungle.init(app_id, getApplicationContext(), new InitCallback() {
            @Override
            public void onSuccess() {
                Log.d(LOG_TAG, "InitCallback - onSuccess");
                    }
            @Override
            public void onError(Throwable throwable) {
                Log.d(LOG_TAG, "InitCallback - onError: " + throwable.getLocalizedMessage());
            }

            @Override
            public void onAutoCacheAdAvailable(final String placementReferenceID) {
                Log.d(LOG_TAG, "InitCallback - onAutoCacheAdAvailable" );
            }
        });
    }


    private final PlayAdCallback vunglePlayAdCallback = new PlayAdCallback() {
        @Override
        public void onAdStart(final String placementReferenceID) {
            Log.d(LOG_TAG, "PlayAdCallback - onAdStart" );

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int index = placementsList.indexOf(placementReferenceID);
                }
            });
        }

        @Override
        public void onAdEnd(final String placementReferenceID, final boolean completed, final boolean isCTAClicked) {

        }

        @Override
        public void onError(final String placementReferenceID, Throwable throwable) {
            Log.d(LOG_TAG, "PlayAdCallback - onError" +
                    "\n\tPlacement Reference ID = " + placementReferenceID +
                    "\n\tError = " + throwable.getLocalizedMessage());

            checkInitStatus(throwable);
        }
    };
    private void checkInitStatus(Throwable throwable) {
        try {
            VungleException ex = (VungleException) throwable;
            Log.d(LOG_TAG, ex.getExceptionCode() + "");

            if (ex.getExceptionCode() == VungleException.VUNGLE_NOT_INTIALIZED) {
                initSDK();
            }
        } catch (ClassCastException ex) {
            Log.d(LOG_TAG, ex.getMessage());
        }
    }

            private final LoadAdCallback vungleLoadAdCallback = new LoadAdCallback() {
                @Override
                public void onAdLoad(final String placementReferenceID) {

                    Log.d(LOG_TAG,"LoadAdCallback - Loading" );

                }

                @Override
                public void onError(final String placementReferenceID, Throwable throwable) {
                    Log.d(LOG_TAG, "LoadAdCallback - onError" );
                    checkInitStatus(throwable);
                }
            };


}
